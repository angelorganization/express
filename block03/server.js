const { application } = require('express');

const express=require('express'),
    app=express(),
    port=process.env.PORT||4040,
    mongoose=require('mongoose'),
    categoryRoute=require('./routes/category'),
    productRoute=require('./routes/product')

app.use(express.urlencoded({extended:true}));
app.use(express.json());

app.use(require("cors")())

const connect=async()=>{
    try{
        await mongoose.connect('mongodb://127.0.0.1/newdatabase');
        console.log('connected to the DB');
    }catch(err){console.error('Not connected to the DB ! You gotta do something');}
}
connect();

app.use('/category',categoryRoute);
app.use('/product',productRoute);

app.listen(port,()=>console.log(`Server listening on port ${port}`));