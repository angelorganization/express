const Categories=require('../models/categories.js')
const Products=require('../models/products.js')

class CategoriesController {
    async add(req,res){
        try{
            const{category}=req.body
            let data
            const found=await Categories.findOne({category:category})
            if(found){
                data=`Category ${category} already exists`
            }else{
                await Categories.create({category})
                data=`Category ${category} added successfully`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async delete(req,res){
        try{
            const{category}=req.body;
            const found=await Categories.findOne({category:category})
            let data
            if(found){
                await Categories.deleteOne({category:category})
                data=`Category ${category} deleted successfully`
            }else{
                data=`Category ${category} doesn't exist`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async update(req,res){
        try{
            const{new_category,old_category}=req.body;
            const found=await Categories.findOne({category:old_category})
            let data
            if(found){
                await Categories.updateOne({category:old_category},{category:new_category})
                data=`Category ${new_category} updated successfully`
            }else{data=`Category ${old_category} doesn't exist`}
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async findCategories(req,res){
        try{
            const all=await Categories.find()
            res.send({ok:true,data:all})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async findCategory(req,res){
        try{
            const {category}=req.params
            const found=await Products.find({category:category})
            let data
            if(found.length>0){data=found}
            else{data=`Category ${category} doesn't exist`}
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
}

module.exports = new CategoriesController();