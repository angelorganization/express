const Products = require('../models/products.js');

class ProductsController {
    async findAll(req,res){
        try{
            const all=await Products.find({})
            res.send({ok:true,data:all})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async add(req,res){
        try{
            const{product,category}=req.body
            let data
            const found=await Products.findOne({name:product.name})
            if(found){
                data=`Product ${product.name} already exists`
            }else{
                const created=await Products.create({name:product.name,price:product.price,color:product.color,description:product.description,category:category})
                data=`Product ${product.name} added successfully`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async delete(req,res){
        try{
            const{product}=req.body;
            const found=await Products.findOne({name:product.name})
            let data
            if(found){
                await Products.deleteOne({name:product.name})
                data=`Product ${product.name} deleted successfully`
            }else{
                data=`Product ${product.name} doesn't exist`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
    async update(req,res){
        try{
            const{new_product,old_product}=req.body
            let data
            const found=await Products.findOne({name:old_product.name})
            if(found){
                await Products.updateOne({name:old_product.name},{name:new_product.name})
                data=`Product ${new_product.name} updated successfully`
            }else{
                data=`Product ${old_product.name} doesn't exist`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err)
            res.send({ok:false,err})
        }
    }
    async findProduct(req,res){
        try{
            const {product}=req.params
            const found=await Products.findOne({name:product})
            let data
            if(found){
                data=found
            }else{
                data=`Product ${product} doesn't exist`
            }
            res.send({ok:true,data:data})
        }catch(err){
            console.error(err);
            res.send({ok:false,err})
        }
    }
}

module.exports = new ProductsController();