const express=require('express'),
    router=express.Router(),
    controller=require('../controllers/product');

router.get("/",controller.findAll);
router.post("/add",controller.add);
router.post("/delete",controller.delete);
router.post("/update",controller.update);
router.get("/:product",controller.findProduct);

module.exports=router;