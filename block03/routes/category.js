const express=require('express'),
    router=express.Router(),
    controller=require('../controllers/category');

router.post("/add",controller.add);
router.post("/delete",controller.delete);
router.post("/update",controller.update);
router.get("/categories",controller.findCategories);
router.get("/:category",controller.findCategory);

module.exports=router;