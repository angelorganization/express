const express=require('express');
const app=express();
const port=process.env.PORT||4040;

app.use(express.urlencoded({extended:true}));
app.use(express.json());

const db=[];

app.get('/category/:search',(req,res)=>{
    const {search}=req.params;
    let data;
    let i=db.findIndex(obj=>obj.category===search);
    if(search==='categories'){
        let cats='';
        db.forEach(obj=>{
            cats+=String(obj.category)+', ';
        })
        data=cats;
    }else if(search==='products'){
        data=db;
    }else if(i>=0){
        data=db[i].products;
    }
    res.send({ok:true,data:data});
});

/*app.post('/category/add/',(req,res)=>{
    const {category,new_category,old_category}=req.body;
    const i=db.findIndex(obj=>obj.category===category);
    let data;
    if(i===-1&&category){
        db.push({category:category});
        data=`Category ${category} added successfully`;
    }else{
        data=category?`Category ${category} already exists`:`input is invalid`;}
    res.send({ok:true,data:data});
});*/

/*app.post('/category/delete',(req,res)=>{
    const {category,new_category,old_category}=req.body;
    const i=db.findIndex(obj=>obj.category===category);
    let data;
    if(i>=0){
        db.splice(i,1);
        data=`Category ${category} deleted successfully`;
    }else{data=`Category ${category} doesn't exist`;}
    res.send({ok:true,data:data});
});*/

//update
app.post('/category/:action',(req,res)=>{
    const {category,new_category,old_category}=req.body;
    const {action}=req.params;
    let i;
    if(action==='update'){ i=db.findIndex(obj=>obj.category===old_category);}
    else{i=db.findIndex(obj=>obj.category===category);}
    let data;
    if(action==='update'){
        if(i>=0){
            db[i].category=new_category;
            data=`Category ${new_category} updated successfully`;
        }else{data=`Category ${old_category} doesn't exist`;}
    }else if(action==='add'){
        if(i===-1&&category){
            db.push({category:category});
            data=`Category ${category} added successfully`;
        }else{
            data=category?`Category ${category} already exists`:`input is invalid`;}
    }else if(action==='delete'){
        if(i>=0){
            db.splice(i,1);
            data=`Category ${category} deleted successfully`;
        }else{data=`Category ${category} doesn't exist`;}
    }
    res.send({ok:true,data:data});
});

app.post('/product/add',(req,res)=>{
    const {category,product}=req.body;
    const i=db.findIndex(obj=>obj.category===category);
    let data;
    //i being greater than 0 means that the category exists inside the database
    if(i>=0){
        //if the category already has a products list
        if(db[i]['products']){
            let j=db[i]['products'].findIndex(obj=>obj.name===product.name);
            //if the product already exists
            if(j>=0){
                data=`product ${product.name} already exists`;
            }else{
                db[i]['products'].push(product);
                data=`product ${product.name} added successfully`;
            }
        //if it doesn't have a products list, create a new one and add the product there
        }else{
            db[i]['products']=[product];
            data=`product ${product.name} added successfully`;
        }
    }else{
        data=`category ${category} doesn't exist`;
    }
    res.send({ok:true,data:data});
});

app.post('/product/delete',(req,res)=>{
    const {category,product}=req.body;
    const i=db.findIndex(obj=>obj.category===category);
    let data;
    //i being greater than 0 means that the category exists inside the database
    if(i>=0){
        //if the category has a products list
        if(db[i]['products']){
            let j=db[i]['products'].findIndex(obj=>obj.name===product.name);
            //if the product exists
            if(j>=0){
                db[i]['products'].splice(j,1);
                data=`product ${product.name} deleted successfully`;
            }else{
                data=`product ${product.name} doesn't exist`;
            }
        //if it doesn't have a products list, do nothing
        }else{
            data=`product ${product.name} doesn't exist`;
        }
    }else{
        data=`category ${category} doesn't exist`;
    }
    res.send({ok:true,data:data});
});

app.post('/product/update',(req,res)=>{
    const {new_product,old_product,category}=req.body;
    const i=db.findIndex(obj=>obj.category===category);
    let data;
    //i being greater than 0 means that the category exists inside the database
    if(i>=0){
        //if the category has a products list
        if(db[i]['products']){
            let j=db[i]['products'].findIndex(obj=>obj.name===old_product.name);
            //if the product exists
            if(j>=0){
                db[i]['products'][j].name=new_product.name;
                data=`product ${new_product.name} updated successfully`;
            }else{
                data=`product ${old_product.name} doesn't exist`;
            }
        //if it doesn't have a products list, do nothing
        }else{
            data=`product ${old_product.name} doesn't exist`;
        }
    }else{
        data=`category ${category} doesn't exist`;
    }
    res.send({ok:true,data:data});
});

app.get('/db',(req,res)=>{
    res.send({ok:true,data:db})
});

app.get('/*',(req,res)=>{
    res.send({ok:true,data:'404 resource not found'});
});

app.listen(port,()=>{
    console.log('***server running on port*** =>> ', port);
})
