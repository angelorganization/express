const express=require('express');
const { get } = require('express/lib/response');
const app=express();
const port=process.env.PORT||4040;

const langs={
    default:"Hello World",
}

app.get('/',(req,res)=>{
    res.send({ok:true,data:langs.default});
});

app.get('/:lang',(req,res)=>{
    const lang=req.params.lang;
    const result=langs[lang];
    let data=result?result:`Hello World in ${lang} not found`;
    res.send({ok:true,data:data})
});

app.get('/:lang/remove',(req,res)=>{
    const lang=req.params.lang;
    let data=lang in langs?`${lang} removed!`:`${lang} not found`;
    delete langs[lang];
    res.send({ok:true,data:data});
});

app.get('/:lang/update/:greeting',(req,res)=>{
    const {lang,greeting}=req.params;
    let prev=langs[lang];
    let data;
    if(prev){
        data=`${lang} updated from '${prev}' to '${greeting}'`;
        langs[lang]=greeting;
    }else{
        data=`${lang} not found`;
    }

    res.send({ok:true,data:data});
});

app.get('/:lang/:greeting',(req,res)=>{
    const {lang,greeting}=req.params;
    const result=langs[lang];
    let data;
    if(result){
        data=`Action forbidden, ${lang} is already present in the system`;
    }else{
        data=`${lang} added with message ${greeting}`;
        langs[lang]=greeting;
    }
    res.send({ok:true,data:data});
});

app.listen(port,()=>{
    console.log('***server running on port*** =>>', port);
});