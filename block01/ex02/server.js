const express=require('express');
const { get } = require('express/lib/response');
const app=express();
const port=process.env.PORT||4040;

const langs={
    default:"Hello World",
    NL:"Hallo Wereld",
    IT:"Ciao Mondo"
}

app.get('/',(req,res)=>{
    res.send({ok:true,data:'Hello World'});
});

app.get('/:lang',(req,res)=>{
    const lang=req.params.lang;
    const result=langs[lang];
    let data=result?result:`Hello World in ${lang} not found`;
    res.send({ok:true,data:data})
});

app.get('/:lang/:greeting',(req,res)=>{
    const {lang,greeting}=req.params;
    const result=langs[lang];
    langs[lang]=greeting;
    let data=result?`${lang} changed message to ${greeting}`:`${lang} added with message ${greeting}`;
    res.send({ok:true,data:data});
});



app.listen(port,()=>{
    console.log('***server running on port*** =>>', port);
});