const express=require('express');
const app=express();
const port=process.env.PORT||4040;

const langs={
    default:"Hello World",
    NL:"Hallo Wereld",
    IT:"Ciao Mondo"
}

app.get('/',(req,res)=>{
    res.send({ok:true,data:'Hello World'})
})

app.get('/:lang',(req,res)=>{
    const lang=req.params.lang;
    const result=langs[lang];
    result?data=result:data='Hello World';
    res.send({ok:true,data:data})
})

app.listen(port,()=>{
    console.log('***server running on port*** =>>', port);
})