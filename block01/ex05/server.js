const express=require('express');
const app=express();
const port=process.env.PORT||4040;

const db=[
    //{ID:'',balance:0}
]

app.get('/account/new/:accountID/:amount',(req,res)=>{
    const {accountID,amount}=req.params;
    let i=db.findIndex((obj)=>obj.ID===accountID);
    let data;
    if(accountID!=Number(accountID)||amount!=Number(amount))return res.send({ok:true,data:'Account not created, both inputs must be numbers'});
    if(i===-1){
        data=`account num ${accountID} created with ${amount} euros`;
        db.push({ID:accountID,balance:amount});
    }else{data=`account ${accountID} already exists`;}
    res.send({ok:true,data:data});
});

const dw=(id,amount,action)=>{
    let i=db.findIndex((obj)=>obj.ID===id);
    let data;
    if(amount!=Number(amount))return {ok:true,data:`${action==='d'?'deposit':'withdraw'} input must be a number`};
    if(i>=0){
        let bal=db[i].balance;
        data=`${amount} euros ${action==='d'?'added to':'taken from'} account num ${id}`;
        db[i].balance=action==='d'?bal+Number(amount):bal-Number(amount);
    }else{data=`Account not found`;}
    return {ok:true,data:data}
}

app.get('/:accountID/:action/:amount',(req,res)=>{
    const {accountID,action,amount}=req.params;
    let act;
    if(action==='withdraw'){act='w';}
    else if(action==='deposit'){act='d';}
    res.send(dw(accountID,amount,act));
});

app.get('/:accountID/balance',(req,res)=>{
    const {accountID}=req.params;
    let i;
    if(db.length>0){
        i=db.findIndex((obj)=>obj.ID===accountID);
    }
    let data=i>=0?`${db[i].balance}`:`Account not found`;
    res.send({ok:true,data:data});
});

app.get('/:accountID/delete',(req,res)=>{
    const {accountID}=req.params;
    let i=db.findIndex((obj)=>obj.ID===accountID);
    let data;
    if(i>=0){
        data=`Account num ${accountID} deleted`;
        delete db[i].ID;
    }
    else{data=`Account not found`;}
    res.send({ok:true,data:data});
});

app.get('/db',(req,res)=>{
    res.send(db);
});

app.get('/*',(req,res)=>{
    res.send({ok:true,data:'404 resource not found'});
});

app.listen(port,()=>{
    console.log('***server running on port*** =>> ', port);
})
