const Todos = require('../models/TodosModel');

class TodosController {
    // GET FIND ALL
    async findAll(req, res){
        try{
            const todos = await Todos.find({});
            res.send(todos);
        }
        catch(e){
            res.send({e})
        }
    }
    // FIND ONE TODO BY _ID
    async findOne(req ,res){
        let { todo_id} = req.params;
        try{
            const todo = await Todos.findOne({_id:todo_id});
            res.send(todo);
        }
        catch(e){res.send({e})}
    }
    // POST ADD ONE
    async insert (req, res) {
        let { todo } = req.body;
        try{
            const done = await Todos.create({todo});
            res.send(done)
        }
        catch(e){res.send({e})}
    }
    // DELETE TODO
    async delete (req, res){
        console.log('delete!!!')
        let { todo } = req.body;
        try{
            const removed = await Todos.deleteOne({ todo });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }
    // UPDATE TODO
    async update (req, res){
        let { todo, newTodo } = req.body;
        try{
            const updated = await Todos.updateOne(
                { todo },{ todo:newTodo }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }
};
module.exports = new TodosController();