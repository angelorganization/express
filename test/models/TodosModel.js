const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const todosSchema = new Schema({
    todo:String
},
// Set `strict: false` or `strictQuery:false` to opt in to filtering by properties that aren't in the schema
{strictQuery: false})
module.exports =  mongoose.model('todos', todosSchema);